﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PracticeAlgorithms
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "Hello World How Are You?";
            Console.WriteLine(ReversedString(str));
            Console.WriteLine(IsPalindrome(str));
            Console.WriteLine(ReverseWordOrder(str));
            Console.WriteLine(ReverseWords(str));
            Countcharacter(str);
            Console.ReadLine();
        }

        // Q.1: How to reverse a string?
        static string ReversedString(string str)
        {
            try
            {
                char[] charArray = str.ToCharArray();
                for (int i = 0, j = str.Length - 1; i < j; i++, j--)
                {
                    charArray[i] = str[j];
                    charArray[j] = str[i];
                }
                string reversedstring = new string(charArray);
                return reversedstring;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        // Q.2: How to find if the given string is a palindrome or not?
        public static string IsPalindrome(string str)
        {
            str = str.ToLower();
            for (int i = 0, j = str.Length - 1; i < str.Length / 2; i++, j--)
            {
                if (str[i] != str[j])
                {
                    return "Not Palindrome";
                }
            }
            return "Palindrome";
        }

        //Q.3: How to reverse the order of words in a given string?
        public static string ReverseWordOrder(string str)
        {
            int i;
            StringBuilder reverseSentence = new StringBuilder();

            int Start = str.Length - 1;
            int End = str.Length - 1;

            while (Start > 0)
            {
                if (str[Start] == ' ')
                {
                    i = Start + 1;
                    while (i <= End)
                    {
                        reverseSentence.Append(str[i]);
                        i++;
                    }
                    reverseSentence.Append(' ');
                    End = Start - 1;
                }
                Start--;
            }

            for (i = 0; i <= End; i++)
            {
                reverseSentence.Append(str[i]);
            }
           return reverseSentence.ToString();
        }

        //Q.4: How to reverse each word in a given string?
        public static string ReverseWords(string str)
        {
            StringBuilder output = new StringBuilder();
            List<char> charlist = new List<char>();

            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == ' ' || i == str.Length - 1)
                {
                    if (i == str.Length - 1)
                        charlist.Add(str[i]);
                    for (int j = charlist.Count - 1; j >= 0; j--)
                        output.Append(charlist[j]);

                    output.Append(' ');
                    charlist = new List<char>();
                }
                else
                    charlist.Add(str[i]);
            }
            return output.ToString();
        }

        //Q.5: How to count the occurrence of each character in a string?
        internal static void Countcharacter(string str)
        {
            Dictionary<char, int> characterCount = new Dictionary<char, int>();

            foreach (var character in str)
            {
                if (character != ' ')
                {
                    if (!characterCount.ContainsKey(character))
                    {
                        characterCount.Add(character, 1);
                    }
                    else
                    {
                        characterCount[character]++;
                    }
                }

            }
            foreach (var character in characterCount)
            {
                Console.WriteLine("{0} - {1}", character.Key, character.Value);
            }
        }
    }
}
